package ru.t1.ktitov.tm.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import lombok.Getter;
import lombok.Setter;
import ru.t1.ktitov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class Task {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    private Date dateStart;

    private Date dateFinish;

    private String projectId;

    public Task(@NotNull final String name) {
        this.name = name;
    }

}
