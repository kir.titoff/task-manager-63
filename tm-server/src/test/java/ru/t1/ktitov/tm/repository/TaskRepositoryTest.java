package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.api.repository.model.ITaskRepository;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.model.IProjectService;
import ru.t1.ktitov.tm.api.service.model.IUserService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.repository.model.TaskRepository;
import ru.t1.ktitov.tm.service.model.ProjectService;
import ru.t1.ktitov.tm.service.model.UserService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.TaskTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Component
@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private EntityManager entityManager;

    @NotNull
    protected ITaskRepository getRepository() {
        return context.getBean(TaskRepository.class);
    }

    private void compareTasks(@NotNull final Task task1, @NotNull final Task task2) {
        Assert.assertEquals(task1.getId(), task2.getId());
        Assert.assertEquals(task1.getName(), task2.getName());
        Assert.assertEquals(task1.getDescription(), task2.getDescription());
        Assert.assertEquals(task1.getStatus(), task2.getStatus());
        Assert.assertEquals(task1.getUser().getId(), task2.getUser().getId());
        Assert.assertEquals(task1.getCreated(), task2.getCreated());
    }

    private void compareTasks(
            @NotNull final List<Task> taskList1,
            @NotNull final List<Task> taskList2) {
        Assert.assertEquals(taskList1.size(), taskList2.size());
        for (int i = 0; i < taskList1.size(); i++) {
            compareTasks(taskList1.get(i), taskList2.get(i));
        }
    }

    @Before
    public void initData() {
        @NotNull final IUserService userService = new UserService();
        userService.add(USER1);
        userService.add(USER2);
        userService.add(ADMIN3);
        @NotNull final IProjectService projectService = new ProjectService();
        projectService.add(USER1_PROJECT_LIST);
        projectService.add(USER2_PROJECT_LIST);
        projectService.add(ADMIN1_PROJECT_LIST);
    }

    @After
    public void tearDown() {
        @NotNull final IUserService userService = new UserService();
        userService.removeById(USER1.getId());
        userService.removeById(USER2.getId());
        userService.removeById(ADMIN3.getId());
    }

    @Test
    @SneakyThrows
    public void add() {
        try {
            @NotNull final ITaskRepository repository = getRepository();
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            Task task = repository.findAll().get(0);
            compareTasks(USER1_TASK1, task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void addList() {
        try {
            @NotNull final ITaskRepository repository = getRepository();
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            Assert.assertEquals(3, repository.findAll().size());
            compareTasks(USER1_TASK1, repository.findAll().get(0));
            compareTasks(USER1_TASK2, repository.findAll().get(1));
            compareTasks(USER1_TASK3, repository.findAll().get(2));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        try {
            @NotNull final ITaskRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            compareTasks(USER1_TASK1, repository.findAll().get(0));
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        try {
            @NotNull final ITaskRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            repository.clear(USER2.getId());
            Assert.assertFalse(repository.findAll().isEmpty());
            repository.clear(USER1.getId());
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            repository.clear(USER2.getId());
            compareTasks(USER1_TASK1, repository.findOneById(USER1_PROJECT1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        try {
            @NotNull final ITaskRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            repository.add(USER2_TASK1);
            compareTasks(USER1_TASK_LIST, repository.findAll(USER1.getId()));
            compareTasks(USER2_TASK_LIST, repository.findAll(USER2.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        try {
            @NotNull final ITaskRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            compareTasks(USER1_TASK1, repository.findOneById(USER1_TASK1.getId()));
            Assert.assertNull(repository.findOneById(USER2.getId(), USER1_TASK1.getId()));
            compareTasks(USER1_TASK1, repository.findOneById(USER1.getId(), USER1_TASK1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        try {
            @NotNull final ITaskRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            repository.remove(USER1_TASK1);
            Assert.assertEquals(2, repository.findAll().size());
            repository.removeById(USER1_TASK2.getId());
            Assert.assertEquals(1, repository.findAll().size());
            compareTasks(USER1_TASK3, repository.findAll().get(0));
            repository.clear();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            Assert.assertEquals(3, repository.findAll().size());
            repository.removeById(USER2.getId(), USER1_TASK1.getId());
            Assert.assertEquals(3, repository.findAll().size());
            repository.removeById(USER1.getId(), USER1_TASK1.getId());
            repository.removeById(USER1.getId(), USER1_TASK2.getId());
            compareTasks(USER1_TASK3, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        try {
            @NotNull final ITaskRepository repository = getRepository();
            entityManager.getTransaction().begin();
            repository.add(USER1_TASK1);
            repository.add(USER1_TASK2);
            repository.add(USER1_TASK3);
            repository.add(USER2_TASK1);
            compareTasks(USER1_TASK_LIST, repository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
