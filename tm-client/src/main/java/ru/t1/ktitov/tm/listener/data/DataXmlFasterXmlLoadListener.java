package ru.t1.ktitov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataXmlFasterXmlLoadRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public class DataXmlFasterXmlLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-xml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file by fasterxml";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlFasterXmlLoadListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataXmlFasterXmlLoadRequest request = new DataXmlFasterXmlLoadRequest(getToken());
        domainEndpoint.loadDataXmlFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
