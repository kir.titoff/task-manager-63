package ru.t1.ktitov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Sort {

    BY_NAME("Sort by name"),
    BY_STATUS("Sort by status"),
    BY_CREATED("Sort by created");

    @NotNull
    private final String displayName;

    Sort(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@Nullable final Sort sort : values()) {
            if (sort == null) continue;
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    @NotNull
    public static String getOrderByField(final Sort sort) {
        if (sort == BY_STATUS) return "status";
        if (sort == BY_CREATED) return "created";
        return "name";
    }

}
