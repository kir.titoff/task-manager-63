package ru.t1.ktitov.tm.service.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.ktitov.tm.api.service.dto.IProjectDtoService;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ktitov.tm.exception.entity.UserNotFoundException;
import ru.t1.ktitov.tm.exception.field.*;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Getter
@Service
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO>
        implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Override
    public ProjectDTO add(@Nullable final ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public Collection<ProjectDTO> add(@Nullable final Collection<ProjectDTO> models) {
        if (models == null) throw new EntityNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public ProjectDTO update(@Nullable final ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> models) {
        clear();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return repository.findAll();
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findByUserId(userId);
    }


    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, Sort.getOrderByField(sort));
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findByIdAndUserId(userId, id);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id).isPresent();
    }

    @Override
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        return repository.existsByIdAndUserId(userId, id);
    }

    @NotNull
    @Override
    public ProjectDTO remove(@Nullable final ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        repository.delete(model);
        return model;
    }

    @NotNull
    @Override
    public ProjectDTO removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Optional<ProjectDTO> model = repository.findById(id);
        if (!model.isPresent()) throw new EntityNotFoundException();
        repository.deleteById(id);
        return model.get();
    }

    @NotNull
    @Override
    public ProjectDTO removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ProjectDTO model = repository.findByIdAndUserId(userId, id);
        if (model == null) throw new ProjectNotFoundException();
        return remove(model);
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public void clear(@Nullable String userId) {
        if (userId == null) throw new EmptyUserIdException();
        @Nullable final List<ProjectDTO> projects = findAll(userId);
        if (projects == null) throw new ProjectNotFoundException();
        repository.deleteAll(projects);
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        add(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUserId(userId);
        update(project);
        return project;
    }

}
