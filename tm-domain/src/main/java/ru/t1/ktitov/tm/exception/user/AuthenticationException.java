package ru.t1.ktitov.tm.exception.user;

public class AuthenticationException extends AbstractUserException {

    public AuthenticationException() {
        super("Error! Authentication problems. Login doesn't exist or locked.");
    }

}
