package ru.t1.ktitov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.ktitov.tm.api.service.dto.ITaskDtoService;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.dto.model.TaskDTO;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ktitov.tm.exception.entity.UserNotFoundException;
import ru.t1.ktitov.tm.exception.field.*;

import java.util.*;

@Service
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO>
        implements ITaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository repository;

    @NotNull
    @Override
    public TaskDTO add(@Nullable final TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public TaskDTO add(@Nullable String userId, @Nullable TaskDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public Collection<TaskDTO> add(@Nullable final Collection<TaskDTO> models) {
        if (models == null) throw new EntityNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public TaskDTO update(@Nullable final TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> models) {
        clear();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return repository.findAll();
    }

    @Override
    public @Nullable List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findByUserId(userId);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, Sort.getOrderByField(sort));
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findByIdAndUserId(userId, id);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id).isPresent();
    }

    @Override
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        return repository.existsByIdAndUserId(userId, id);
    }

    @NotNull
    @Override
    public TaskDTO remove(@Nullable final TaskDTO model) {
        if (model == null) throw new EntityNotFoundException();
        repository.delete(model);
        return model;
    }

    @NotNull
    @Override
    public TaskDTO removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Optional<TaskDTO> model = repository.findById(id);
        if (!model.isPresent()) throw new EntityNotFoundException();
        repository.deleteById(id);
        return model.get();
    }

    @NotNull
    @Override
    public TaskDTO removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final TaskDTO model = repository.findByIdAndUserId(userId, id);
        if (model == null) throw new TaskNotFoundException();
        return remove(model);
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public void clear(@Nullable String userId) {
        if (userId == null) throw new EmptyUserIdException();
        @Nullable final List<TaskDTO> tasks = findAll(userId);
        if (tasks == null) throw new TaskNotFoundException();
        repository.deleteAll(tasks);
    }
    
    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        add(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable List<TaskDTO> tasks = repository.findByUserIdAndProjectId(userId, projectId);
        return Optional.ofNullable(tasks).orElse(Collections.emptyList());
    }

}
