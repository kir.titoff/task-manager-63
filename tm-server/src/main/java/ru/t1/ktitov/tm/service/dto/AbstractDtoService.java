package ru.t1.ktitov.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.service.dto.IDtoService;
import ru.t1.ktitov.tm.dto.model.AbstractModelDTO;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDTO> implements IDtoService<M> {
}
